package com.apantri.affirmations.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Affirmation(@StringRes val stringResurceId: Int,@DrawableRes val imageResourceId: Int)
